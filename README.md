This project is used by fluxcd to manage resources defined and deployed in the kubernetes cluster.

For the deployment defined in this repo to work properly, the secret with gitlab's Container Registry should be previously created in the default namespace.